/*
 * TUNA-MEGA-BOOT.c
 * 
 * Bootloader application for TUNAMEGA board
 * CCS compiler
 * V1.0
 */
#define _bootloader
#include "../tunaboard/tunamega.h"
#use rs232(baud=115200,parity=N,UART1,bits=8)
#include "TUNA-LOADER.c"





#INT_GLOBAL
void isr(void){
	jump_to_isr(LOADER_END+9);
}

#org LOADER_END+2, LOADER_END+4
void application(void) {
  while(TRUE);
}

void main()
{
    output_low(LED1);
    output_low(LED2);

	// Enter Bootloader if Pin B0 is low after a RESET
	if(input(BOOT))
	{
		output_high(LED1);
        output_high(LED2);
        load_program();
	}

	application();

}
