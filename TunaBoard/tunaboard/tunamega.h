/* 
 * TUNABOARD PROJECT
 * TUNA MEGA
 * Libreria con definiciones de pines
 * bootloader
 * pic18f67k40
 * 
 */

#include <18F67K40.h>
#device ADC = 10
#include <bootloader.h>

//Leds
#define LED1 PIN_D3
#define LED2 PIN_D4
//Botones
#define BOOT PIN_B0
#define USER2 PIN_B1

 //UART 1
#define UART_TX1 PIN_C6
#define UART_RX1 PIN_C7
//UART 2 
#define UART_TX2 PIN_G1
#define UART_RX2 PIN_G2
//UART 3 
#define UART_TX3 PIN_E0
#define UART_RX3 PIN_E1
//UART 4 
#define UART_TX4 PIN_C0
#define UART_RX4 PIN_C1
//UART 5 
#define UART_TX5   PIN_E2
#define UART_RX5   PIN_E3


//I2C 1
#define I2C_SCL1 PIN_D6
#define I2C_SDA1 PIN_D5

//I2C 2
#define I2C_SCL2 PIN_C3
#define I2C_SDA2 PIN_C4

#pin_select U1TX=UART_TX1
#pin_select U1RX=UART_RX1

#pin_select U2TX=UART_TX2
#pin_select U2RX=UART_RX2

#pin_select U3TX=UART_TX3
#pin_select U3RX=UART_RX3

#pin_select U4TX=UART_TX4
#pin_select U4RX=UART_RX4

#pin_select U5TX=UART_TX5
#pin_select U5RX=UART_RX5

#use delay(crystal=20mhz)