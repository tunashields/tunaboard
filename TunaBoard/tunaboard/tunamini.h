/* 
 * TUNABOARD PROJECT
 * TUNA MINI
 * Libreria con definiciones de pines
 * bootloader
 * 18F46K22
 * 
 */

#include <18F46K22.h>
#include <bootloader.h>

//Leds
#define LED1 PIN_C2
#define LED2 PIN_C1
//Botones
#define BOOT PIN_B0
//#define USER2 PIN_B1

 //UART 1
#define UART_TX1 PIN_C6
#define UART_RX1 PIN_C7
//UART 2 
#define UART_TX2 PIN_D6
#define UART_RX2 PIN_D7

//I2C 1
#define I2C_SCL1 PIN_C4
#define I2C_SDA1 PIN_C3


#use delay(crystal=20mhz)