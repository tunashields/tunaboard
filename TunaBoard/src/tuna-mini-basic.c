/*
 * Project: TUNA-MINI
 * Description: TUNABOARD EXAMPLE
 * Author: Arturo Gasca
 * Date:  November-2019
 * Development: Tuna Shields
 *
 */


#include "../tunaboard/tunamini.h"


#define COMPILACION_DATE    __DATE__
#define COMPILACION_TIME    __TIME__
#define FW_VERSION  1.0
#define HW_NAME "TUNA-MINI"




#use rs232(baud=115200,parity=N,UART2,bits=8,stream=USB)
//#use rs232(baud=115200,parity=N,UART2,bits=8,stream=DEBUG)
//#use rs232(baud=115200,parity=N,UART3,bits=8,stream=SERIAL_TTL)
//#use rs232(baud=115200,parity=N,UART4,bits=8,stream=SERIAL_232)
//#use rs232(baud=115200,parity=N,UART5,bits=8,stream=MODBUS)

//#use rs232(baud=115200,parity=N,UART2,bits=8,stream=DEBUG,restart_wdt)



/*
#INT_TIMER1
void TIMER1_isr(void){
    timer_counter++;
    if(timer_counter >= 10){
        segundos++;
        if(segundos >= time_stamp){
            flag_time = 1;
            segundos =0;
        }
        timer_counter = 0;
    }
    set_timer1(3036);
}*/



void main (void){
    set_tris_b(0b00000001);
    set_tris_c(0b10000000);
    set_tris_d(0b10000000);
    set_tris_e(0b00001000);
    
    setup_timer_1(T1_INTERNAL|T1_DIV_BY_8);      //104 ms overflow
    set_timer1(3036);
    delay_ms(3000);
    fprintf(USB,"\r\n<< %s: V%f %s %s >>\r\n",HW_NAME,FW_VERSION,COMPILACION_DATE,COMPILACION_TIME);
  
    
    enable_interrupts(INT_TIMER1);
	
	delay_ms(1000);
    
    
    while(true){
        fprintf(USB,"Hola mundo\r\n");
		output_toggle(LED1);
		output_toggle(LED2);
		delay_ms(1000);               		       
    }
}


