#include <18F46K22.h>
#device ADC=16

#FUSES NOWDT                 	//No Watch Dog Timer
#FUSES WDT128                	//Watch Dog Timer uses 1:128 Postscale
#FUSES NOBROWNOUT            	//No brownout reset
#FUSES NOLVP                 	//No low voltage prgming, B3(PIC16) or B5(PIC18) used for I/O
#FUSES NOXINST               	//Extended set extension and Indexed Addressing mode disabled (Legacy mode)

//#device ICD=TRUE
#use delay(crystal=20000000)
#use rs232(baud=115200,parity=N,xmit=PIN_C6,rcv=PIN_C7,bits=8,stream=PORT1)


#define _bootloader

// NOTE - User must include bootloader.h in application program
#include <bootloader.h>
#include <loader_tunaboard.c>

#define PUSH_BUTTON PIN_A5

#INT_GLOBAL
void isr(void){
	jump_to_isr(LOADER_END+9);
}

#org LOADER_END+2, LOADER_END+4
void application(void) {
  while(TRUE);
}

void main()
{
    output_low(pin_b0);
    output_low(pin_b1);

	// Enter Bootloader if Pin B0 is low after a RESET
	if(!input(PUSH_BUTTON))
	{
		output_high(pin_b0);
        output_high(pin_b1);
        load_program();
	}

	application();

}
