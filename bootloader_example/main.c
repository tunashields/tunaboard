#include <main.h>
 
#define _bootloader

// NOTE - User must include bootloader.h in application program
#include <bootloader.h>
#include <loader.c>

#define PUSH_BUTTON PIN_B0

#INT_GLOBAL
void isr(void){
	jump_to_isr(LOADER_END+9);
}

#org LOADER_END+2, LOADER_END+4
void application(void) {
  while(TRUE);
}

void main()
{
 
	// Enter Bootloader if Pin B0 is low after a RESET
	if(!input(PIN_B0))
	{
		load_program();
	}
	
	application();

}
